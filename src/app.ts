// Recovery of elements
const form = document.querySelector<HTMLFormElement>("form")!;
const ageInput = document.querySelector<HTMLInputElement>("#age")!;
const relationInput = document.querySelector<HTMLSelectElement>("#relation")!;
const themeInput = document.querySelector<HTMLInputElement>("#theme")!;
const submitButton = document.querySelector<HTMLButtonElement>("button")!;
const footer = document.querySelector<HTMLElement>("footer")!;
const OPENAI_API_KEY = "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";

//This function allows you to display a loading by hearing the loading of the response
const setLoadingItems = () => {
  footer.textContent = "Chargement de supers idées en cours !";
  footer.setAttribute("aria-busy", "true");
  submitButton.setAttribute("aria-busy", "true");
  submitButton.disabled = true;
};

//This function allows you to deactivate the loading
const removeLoadingItems = () => {
  footer.setAttribute("aria-busy", "false");
  submitButton.setAttribute("aria-busy", "false");
  submitButton.disabled = false;
};

//This function allows you to generate the text to pass to the AI
const generatePromptText = (age: number, themes = "", relation = "") => {
  let prompt = `Propose moi, avec un ton joyeux et amical, 5 idées de cadeau de noél pour mon condition  âgée de ${age} ans `;
  if (relation.trim()) {
    prompt += ` pour mon condition  ${relation}`;
  }
  if (themes.trim()) {
    prompt += ` et qui aime ${themes}`;
  }
  return prompt + " !";
};

//Processing of the request
form.addEventListener("submit", (e: SubmitEvent) => {
  e.preventDefault();
  setLoadingItems();
  fetch(`https://api.openai.com/v1/completions`, {
    method: "POST",
    headers: {
      "content-Type": "application/json",
      Authorization: `Bearer ${OPENAI_API_KEY}`,
    },
    body: JSON.stringify({
      prompt: generatePromptText(
        ageInput.valueAsNumber,
        themeInput.value,
        relationInput.value
      ),
      max_tokens: 2500,
      model: "text-davinci-003",
    }),
  })
    .then((response) => response.json())
    .then((data) => {
      footer.innerHTML = translateTextToHtml(data.choices[0].text);
    })
    .finally(() => {
      removeLoadingItems();
    });
});
//This function translates the AI ​​response into html
const translateTextToHtml = (text: string) =>
  text
    .split("\n")
    .map((str) => `<p>${str}</p>`)
    .join("");
